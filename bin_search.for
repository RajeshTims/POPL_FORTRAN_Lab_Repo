c		this program does binary search
		
		program binSearch
		dimension arr(10)
c		declare a array data type

		integer arr
c		declare the data types of the array
		integer value
c		init the data on that array
		data arr /1,2,3,4,5,6,7,8,9,10/
		write(*,*)'enter value to search'
		read *, value
c		read the value to be searched in the array
		integer low,high,mid
		low =1
c 		initially low is 1 and high is the no. of elements on that array
		high = 10
c		if high-low<0, then terminate the program

400		if(high-low)500,600,600

c		simple logic here
600		mid = (low + high)/2
		
		if(arr(mid)-value)300,100,200
c		start searching in the lower part of the array
300		high = mid -1
		goto 400
c		start searching in the upper part of the array
200		low = mid +1
		goto 400
100		write(*,*)'Found'
		goto 700
500		write(*,*)'Not found'
700		end;		
		



