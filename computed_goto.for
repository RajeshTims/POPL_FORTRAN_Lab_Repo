c		computed goto

		program example
		integer k
		read *,k
		goto(10,20,30,40),k
c   	k=1 => goto 10, k=2 => goto 20 and so on
c 		this makes everything clear

10		write(*,*)'10'
20		write(*,*)'20'
30		write(*,*)'30'
40		write(*,*)'40'
		end
