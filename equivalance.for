c		this program uses exclusive data a,b		
		program equi 
		dimension a(10)
		dimension b(10)
		equivalence(a(1),b(1))
		a(5) = 56
c		this aliases mem addr of a(1) to b(1)
c		so that even though there are two array, they point to the same memory location
		write(*,*)b(5)
		end