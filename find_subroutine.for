c		this prog finds the third side(H) of the triangle

		program abc
		real p,b,h

		write(*,*)'Enter p and b'
		read *,p,b

		call calc_hyp(p,b,h) 
c 		this calls the suboroutine call_hyp, and passed arguments p,b,h
		write(*,*)'Hyp is',h
		end

c 		this is the definition of the subroutine
		subroutine calc_hyp(x,y,output)
			real x,y,output
			output = sqrt (x**2+y**2)
c 		here the argument output is used as output variable
			return
			end