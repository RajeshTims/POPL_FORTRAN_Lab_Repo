c		this prog finds the area of circle

		program a
c 		this declares that area, radius are to be shared
		real area,radius
		common area,radius

		write(*,*)'Enter radius of circle'
		read *, radius
		call area_of_circle
		write(*,*)'Area of circle is: ',area
		end

		subrotine area_of_circle
c 		wherever we need the shared/common data,we repeat the same statement that was used to declare the common block
			common area,radius
			area = 3.14*radius**2.0
		return 
		end
